﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCreate : MonoBehaviour
{

    public float distanceFromPlayer;
    public GameObject storeButton, store;
    public GameObject smg, shotgun, sniper;
    public int buyableHealthAmount;

    private PlayerController player;
    private bool storeOpened;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(player.transform.position, transform.position) <= distanceFromPlayer)
        {
            // unlock and show the mouse when your close enough (only if not on mobile)
            if (!Application.isMobilePlatform)
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }

            // display the store if it's been opened- else display the open button
            if (storeOpened)
            {
                storeButton.SetActive(false);
                store.SetActive(true);
            }
            else
            {
                storeButton.SetActive(true);
                store.SetActive(false);
            }
        }
        else
        {
            // lock and hide the mouse if your not close enough, only when the player's alive though
            if (!Application.isMobilePlatform && !player.dead)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }

            // hide the UI when your not close enough
            storeButton.SetActive(false);
            store.SetActive(false);
            storeOpened = false;
        }
    }

    // only needed for the buttons on click
    public void OpenStore()
    {
        storeOpened = true;
    }
    public void CloseStore()
    {
        storeOpened = false;
    }

    // I hate that I had to make each weapon purchase a different function
    // but I had to so I could make the prices correctly change
    //
    // NOTE: Prices are set in the inspector where you choose the OnClickEvent on the buttons

    // if the player has enough points it will set their weapon to the new one and take away the points
    public void PurchaseSmg(int price)
    {
        if (player.weapon && player.weapon.name == "SMG")
        {
            Debug.Log("Player already owns the smg");
            return;
        }

        if (player.score >= price)
        {
            player.score -= price;
            player.SetWeapon(smg);
            Debug.Log("Player successully purchased the smg");
        }
        else
        {
            Debug.Log("Player does not have enough points to purchase the smg");
        }
    }
    // if the player has enough points it will set their weapon to the new one and take away the points
    public void PurchaseShotgun(int price)
    {
        if (player.weapon && player.weapon.name == "Shotgun")
        {
            Debug.Log("Player already owns the shotgun");
            return;
        }

        if (player.score >= price)
        {
            player.score -= price;
            player.SetWeapon(shotgun);
            Debug.Log("Player successully purchased the shotgun");
        }
        else
        {
            Debug.Log("Player does not have enough points to purchase the shotgun");
        }
    }
    // if the player has enough points it will set their weapon to the new one and take away the points
    public void PurchaseSniper(int price)
    {
        // early out if the player already owns that weapon
        if (player.weapon && player.weapon.name == "Sniper")
        {
            Debug.Log("Player already owns the sniper");
            return;
        }

        if (player.score >= price)
        {
            player.score -= price;
            player.SetWeapon(sniper);
            Debug.Log("Player successully purchased the sniper");
        }
        else
        {
            Debug.Log("Player does not have enough points to purchase the sniper");
        }
    }
    // heals the player and takes away the points when successfully bought
    public void PurchaseHealth(int price)
    {
        // early out if players health is already at max
        if (player.health >= player.maxHealth)
        {
            Debug.Log("Player already has full health");
            return;
        }

        if (player.score >= price)
        {
            player.score -= price;
            player.Heal(buyableHealthAmount);
            Debug.Log("Player successully purchased the health");
        }
        else
        {
            Debug.Log("Player does not have enough points to purchase the health");
        }
    }

}
