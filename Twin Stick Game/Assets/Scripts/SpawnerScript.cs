﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;


/// <summary>
/// Script to spawn enemies and wave management.
/// </summary>
public class SpawnerScript : MonoBehaviour
{
    public GameObject prefabToSpawn;
    public float spawnTime;
    private float spawnTimer;
    public float numberOfEnemies;

    private float enemiesLeftToSpawn;
    private float enemiesPerWave = 5f;

    public float waveDelay = 5f;

    private float waveCounter = 0;

    private bool waveActive = false;

    //spawner positions:  
    //  Going clockwise from bottom of center/where player spawns;  
    public Transform spawnLocation1;//= new Vector3(10,0,-29);
    public Transform spawnLocation2;//= new Vector3(-49,0,-15);
    public Transform spawnLocation3;//= new Vector3(-49,0,12);
    public Transform spawnLocation4;//= new Vector3(-20,0,27);
    public Transform spawnLocation5;//= new Vector3(16,0,24);
    public Transform spawnLocation6;//= new Vector3(22,0,-15);

    List<Transform> spawners = new List<Transform>();

    // Start is called before the first frame update
    void Start()
    {
        spawners.Add(spawnLocation1);
        spawners.Add(spawnLocation2);
        spawners.Add(spawnLocation3);
        spawners.Add(spawnLocation4);
        spawners.Add(spawnLocation5);
        spawners.Add(spawnLocation6);

        StartNextWave();
    }

    // Update is called once per frame
    void Update()
    {
        spawnTimer -= Time.deltaTime;
        if (waveActive && enemiesLeftToSpawn != 0)
        {
            if (spawnTimer <= 0.5f)
            {
                SpawnEnemy();
            }

        }
        if (enemiesLeftToSpawn == 0)
        {
            Debug.Log("Finished Spawning");
        }
        if (numberOfEnemies == 0)
        {
            waveActive = false;
            WaveCountdown();
            Debug.Log("No More Enemies");

        }


    }

    //Spawning the Zombies at a randomly selected transform from a list of Transform
    void SpawnEnemy()
    {
        GameObject newZombie = Instantiate(prefabToSpawn, spawners[Random.Range(0, 6)].position, Quaternion.identity);
        newZombie.transform.parent = this.transform;


        numberOfEnemies++;
        enemiesLeftToSpawn--;
        ResetSpawnTimer();
    }

    //resets the spawn timer to allow it to count down again.
    void ResetSpawnTimer()
    {
        spawnTimer = 1f;
    }

    void WaveCountdown()
    {
        waveDelay -= Time.deltaTime;
        if (waveDelay <= 0)
        {
            StartNextWave();
        }
    }
    void StartNextWave()
    {

        waveActive = true;
        waveCounter++;
        enemiesPerWave = Mathf.RoundToInt((waveCounter * 2.5f) + 5);
        enemiesLeftToSpawn = enemiesPerWave;
        waveDelay = 5f;
    }

}