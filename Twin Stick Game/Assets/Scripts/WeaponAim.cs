﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAim : MonoBehaviour
{
    public Transform target;
    public Transform camPos;

    // Update is called once per frame
    void Update()
    {
        // transforms the rotation to point toward the target only on the y axis
        Vector3 dir = target.position - camPos.position;
        float angle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
    }
}
