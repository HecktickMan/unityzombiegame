﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{

    public int score;
    [Space]
    public float moveSpeed;
    public int maxHealth;
    public int health = 10;
    public bool dead = false;
    [Space]
    public Transform weaponParent;
    public GameObject weapon;
    [Space]
    public Animator spriteAnim;
    public SpriteRenderer sprite;
    public Joystick joystick;
    public GameObject mobileControls;

    private Rigidbody rb;
    private Vector3 movement;

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;
        rb = GetComponent<Rigidbody>();

        if (!Application.isMobilePlatform) // get rid of joysticks if on the pc
        {
            mobileControls.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

        // so it moves in the direction of input
        if (Application.isMobilePlatform) // mobile input
        {
            movement.x = joystick.Horizontal;
            movement.z = joystick.Vertical;
        }
        else // pc input
        {
            movement.x = Input.GetAxisRaw("Horizontal");
            movement.z = Input.GetAxisRaw("Vertical");
        }

        movement.Normalize();

        // sprite flipping
        if (movement.x > 0)
        {
            sprite.flipX = true;
        }
        else if (movement.x < 0)
        {
            sprite.flipX = false;
        }

        // animation
        if (movement != Vector3.zero)
        {
            spriteAnim.SetBool("Running", true);
        }
        else
        {
            spriteAnim.SetBool("Running", false);
        }
    }

    private void LateUpdate()
    {
        // set the rigidbody's velocity
        rb.velocity = movement * moveSpeed;
    }

    // damages the player, will set the player to dead if health is zero
    public void TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            health = 0;
            dead = true;
        }
    }

    // adds health to the player, will not over heal
    public void Heal(int healAmount)
    {
        health += healAmount;
        if (health > maxHealth)
        {
            health = maxHealth;
        }
    }

    public void SetWeapon(GameObject newWeapon)
    {
        // removes the previous weapon and creates a new one as a child of the weapon parent
        if (weapon)
        {
            Destroy(weapon);
        }
        weapon = Instantiate(newWeapon, weaponParent.position, weaponParent.rotation);
        weapon.transform.parent = weaponParent;
    }
}
