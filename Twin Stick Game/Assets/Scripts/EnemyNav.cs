﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


/// <summary>
/// Enemy navigation script
/// Enemies locating player positions through the intergrated navMesh and navmesh functions.
/// </summary>
public class EnemyNav : MonoBehaviour
{

    private Transform player;
    private NavMeshAgent agent;


    public SpriteRenderer enemySprite;
    public Animator spriteAnim;


    public float health;
    public float moveSpeed;

    
    //private Rigidbody rb;
    

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        agent = GetComponent<NavMeshAgent>();
        
        
    }

    // Update is called once per frame
    void Update()
    {
        agent.updateRotation = false;
        agent.destination = player.position;

        

        if (agent.velocity.x > 0)
        {
            enemySprite.flipX = true;
        }
        else if (agent.velocity.x < 0)
        {
            enemySprite.flipX = false;
        }


        // animation
        if (agent.velocity != Vector3.zero)
        {
            spriteAnim.SetBool("Running", true);
        }
        else
        {
            spriteAnim.SetBool("Running", false);
        }

        if (agent.remainingDistance < agent.stoppingDistance)
        {
            agent.velocity = Vector3.zero;
            agent.isStopped = true;
        }
        else
        {
            agent.isStopped = false;
        }

        

    }
}
