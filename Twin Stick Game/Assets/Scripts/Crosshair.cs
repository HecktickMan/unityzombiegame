﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour
{

    public float sensitivity;
    public Camera playerCamera;
    public SpriteRenderer sr;
    public Joystick joystick;

    Vector3 movement;

    // Update is called once per frame
    void Update()
    {

        // calculates the position to in front of the camera's near plane
        // moves it with the mouse input (not position) and clamps it 
        // at a maximum distance on the screen
        //
        // holy crap this is awful
        float nearPlane = playerCamera.nearClipPlane;

        if (Application.isMobilePlatform) // mobile input
        {
            if (joystick.Direction != Vector2.zero)
            {
                movement.x = joystick.Horizontal * Time.deltaTime * sensitivity * nearPlane;
                movement.z = joystick.Vertical * Time.deltaTime * sensitivity * nearPlane;
            }
            movement = movement.normalized * 0.15f;
        }
        else // pc input
        {
            movement.x += Input.GetAxis("Mouse X") * Time.deltaTime * sensitivity * nearPlane;
            movement.z += Input.GetAxis("Mouse Y") * Time.deltaTime * sensitivity * nearPlane;

            movement = Vector3.ClampMagnitude(movement, 0.15f);
        }
    }

    private void LateUpdate()
    {
        // updates the position to the pre calculated position
        transform.position = playerCamera.transform.position + movement + new Vector3(0f, -(playerCamera.nearClipPlane + 0.05f), 0f);
    }
}
