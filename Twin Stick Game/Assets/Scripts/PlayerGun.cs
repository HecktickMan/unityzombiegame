﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGun : MonoBehaviour
{
    public float timeBetweenShots;
    public float bloom;
    public int bulletsPerShot = 1;
    [Space]
    public Transform bulletSpawn;
    public GameObject bullet;
    [Space]
    public SpriteRenderer weaponSprite;

    private Joystick joystick;
    private float timeSinceShot;

    // Start is called before the first frame update
    void Start()
    {
        try
        {
            joystick = GameObject.FindGameObjectWithTag("Right Joystick").GetComponent<Joystick>();
        }
        catch
        {
            Debug.Log("Couldn't find the right joystick.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        // flips the sprite if the parents rotation is to the right
        if (transform.parent.rotation.eulerAngles.y < 180)
        {
            weaponSprite.flipY = true;
        }
        else
        {
            weaponSprite.flipY = false;
        }


        // the shooting
        if (timeSinceShot > timeBetweenShots)
        {
            if (Application.isMobilePlatform) // mobile input
            {
                if (joystick.Horizontal != 0f || joystick.Vertical != 0f)
                {
                    Shoot();
                }
            }
            else if (Input.GetMouseButton(0)) // pc input
            {
                Shoot();
            }
        }
        else
        {
            timeSinceShot += Time.deltaTime;
        }
    }


    // shoots the gun
    private void Shoot()
    {
        // the for loop is for multishot weapons
        for (int i = 0; i < bulletsPerShot; ++i)
        {
            // spawn a bullet at the spawn position
            GameObject spawnedBullet = Instantiate(bullet, bulletSpawn.position, bulletSpawn.rotation);

            // add bloom to the bullet
            spawnedBullet.transform.Rotate(new Vector3(0f, Random.Range(-bloom, bloom), 0f));
        }
        // reset the timer between shots
        timeSinceShot = 0f;
    }
}
