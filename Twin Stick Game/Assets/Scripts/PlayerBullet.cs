﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{
    public List<float> speeds;
    public int damage;
    private float timeUntilDestroyed = 5f;

    private float speed;

    private void Start()
    {
        // give the bullet a random speed from the lists of speeds
        speed = speeds[Random.Range(0, speeds.Count)];
    }

    // Update is called once per frame
    void Update()
    {
        // move forward at the desired speed
        transform.Translate(Vector3.forward * Time.deltaTime * speed);

        // Destroy itself if the timer's zero
        if (timeUntilDestroyed <= 0f)
        {
            Destroy(gameObject);
        }
        else
        {
            timeUntilDestroyed -= Time.deltaTime;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // check to see if it hit an enemy- if so damage them
        if (other.CompareTag("Enemy"))
        {
            other.GetComponent<EnemyHealth>().TakeDamage(damage);
        }

        // destroy itself if it hit anything other than the player or a bullet
        if (!other.CompareTag("Player") && !other.CompareTag("Bullet"))
        {
            Destroy(gameObject);
        }
    }
}
